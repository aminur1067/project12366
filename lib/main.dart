import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.all(20),
                child: Image.asset('assets/images/software-engineer-hero.jpg'),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                child: Text(
                  'Dvit worner',
                  style: TextStyle(color: Colors.blue),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                child: Text(
                  'SOFTWARE ENGINEER',
                  style: TextStyle(color: Colors.orange),
                ),

              ),
              Container(
                padding: EdgeInsets.all(10),
                child: Text('a book or other written or printed work, regarded in terms of its content rather than its physical form',style: TextStyle(color: Colors.deepPurple),
                ),
              ),
              SizedBox(height: (20)),
              Row(
                children: [
                  const SizedBox(width: (90)),
                  Image.asset(
                    'assets/images/facebook.png',
                    height: 50,
                    width: 50,
                  ),
                  const SizedBox(width: (20)),
                  Image.asset(
                    'assets/images/apple.png',
                    height: 50,
                    width: 50,
                  ),
                  const SizedBox(width: (20)),
                  Image.asset(
                    'assets/images/google.png',
                    height: 50,
                    width: 50,
                  )
                ],
              ),
              SizedBox(height: (10)),
              TextField(

              decoration: InputDecoration(
                border: OutlineInputBorder(

                  borderRadius: BorderRadius.circular(20)
                ),
                prefixIcon: Icon(Icons.phone),


              ),




              ),
              SizedBox(height: 10,),
              Container(
             child: TextField(decoration: InputDecoration(
               border: OutlineInputBorder(

                   borderRadius: BorderRadius.circular(20)
               ),
               prefixIcon: Icon(Icons.mail),),
              )
              )]
          ),
        ),
      ),
    );
  }
}
